Name:           nvwa
Version:        0.2
Release:        6
Summary:        a tool used for openEuler kernel update

License:        MulanPSL-2.0 and Apache-2.0 and MIT and MPL-2.0
URL:            https://gitee.com/openeuler/nvwa
Source:         %{name}-v%{version}.tar.gz
#source1 version sys@v0.0.0-20220908164124-27713097b956
Source1:        sys.tar.gz

Patch1:         0001-ignore-pin-memory-init-in-oe24.03.patch
Patch2:         0002-systemd-solve-the-problem-of-parsing-pid-of-message.patch
Patch3:         0003-nvwa-let-nvwa-to-parse-help-h-and-remove-version-pri.patch

BuildRequires:  golang >= 1.13
BuildRequires:  systemd
Requires:       kexec-tools criu
Requires:       systemd-units iptables-services iproute
Requires:       gcc

%description
A tool used to automate the process of seamless update of the openEuler.

%undefine _missing_build_ids_terminate_build

%prep
%autosetup -n %{name}-v%{version} -p1
%ifarch loongarch64
rm -rf src/vendor/golang.org/x/sys
tar -xf %{SOURCE1} -C src/vendor/golang.org/x/
%endif

%build

cd src
go build -mod=vendor -buildmode=pie
cd -

cd tools/pin
gcc %{name}-pin.c -o %{name}-pin -fstack-protector-all -fPIE -pie -Wl,-z,noexecstack,-z,relo,-z,now
cd -

%install

mkdir -p %{buildroot}/%{_bindir}
mkdir -p %{buildroot}/etc/%{name}
mkdir -p %{buildroot}/etc/%{name}/log
mkdir -p %{buildroot}/usr/lib/systemd/system
mkdir -p %{buildroot}/var/%{name}
mkdir -p %{buildroot}/var/%{name}/running

install -m 0750 %{_builddir}/%{name}-v%{version}/src/%{name} %{buildroot}/%{_bindir}/
install -m 0750 %{_builddir}/%{name}-v%{version}/tools/pin/%{name}-pin %{buildroot}/%{_bindir}/
install -m 0640 %{_builddir}/%{name}-v%{version}/src/config/%{name}-restore.yaml %{buildroot}/etc/%{name}/
install -m 0640 %{_builddir}/%{name}-v%{version}/src/config/%{name}-server.yaml %{buildroot}/etc/%{name}/

install -m 0750 %{_builddir}/%{name}-v%{version}/misc/%{name}-pre.sh %{buildroot}/%{_bindir}/
install -m 0644 %{_builddir}/%{name}-v%{version}/misc/%{name}.service %{buildroot}/usr/lib/systemd/system
install -m 0644 %{_builddir}/%{name}-v%{version}/misc/%{name}-pre.service %{buildroot}/usr/lib/systemd/system

install -d $RPM_BUILD_ROOT/usr/share/bash-completion/completions
install -p -m 0644 %{_builddir}/%{name}-v%{version}/completion/nvwa %{buildroot}/usr/share/bash-completion/completions/nvwa

%post
%systemd_post %{name}.service
%systemd_post %{name}-pre.service
source /usr/share/bash-completion/completions/nvwa

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%license LICENSE
%dir /etc/%{name}/
%dir /etc/%{name}/log
%dir /var/%{name}
%dir /var/%{name}/running
/etc/%{name}/%{name}-restore.yaml
/etc/%{name}/%{name}-server.yaml
/usr/lib/systemd/system/%{name}.service
/usr/lib/systemd/system/%{name}-pre.service
/usr/share/bash-completion/completions/nvwa
%{_bindir}/%{name}
%{_bindir}/%{name}-pin
%{_bindir}/%{name}-pre.sh

%changelog
* Wed May 15 2024 renoseven <dev@renoseven.net> - 0.2-6
- disable pin memory in oe24.03
* Thu Jul 13 2023 huajingyun <huajingyun@loongson.cn> - 0.2-5
- add loong64 support
* Tue Jan 03 2023 anatasluo <luolongjun@huawei.com> - 0.2-4
- Solve the problem of parsing pid of message
* Thu Dec 15 2022 fushanqing <fushanqing@kylinos.cn> - 0.2-3
- Enable debuginfo for fix strip
* Wed Dec 14 2022 anatasluo <luolongjun@huawei.com> - 0.2-2
- Fix issues caused by ctrl-c and systemd
* Fri Jul 30 2021 anatasluo <luolongjun@huawei.com> - 0.2-1
- Update to 0.2
* Fri Mar 19 2021 snoweay <snoweay@163.com> - 0.1-3
- Add secure compile args
* Thu Mar 18 2021 anatasluo <luolongjun@huawei.com> 0.1-2
- Update to 0.1-r2
* Thu Feb 18 2021 anatasluo <luolongjun@huawei.com> - 0.0.1-1
- Update to 0.0.1
